/*
 * Copyright (c) 2011-2014 Jan Dittberner
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package info.dittberner.bcsmime_demo;

import org.bouncycastle.cms.RecipientInformation;
import org.bouncycastle.cms.RecipientInformationStore;
import org.bouncycastle.cms.jcajce.JceKeyTransEnvelopedRecipient;
import org.bouncycastle.cms.jcajce.JceKeyTransRecipientId;
import org.bouncycastle.mail.smime.SMIMEEnveloped;

import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import java.io.ByteArrayInputStream;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Enumeration;

/**
 * S/MIME encryption using the new BouncyCastle 1.46 APIs.
 * 
 * @author Jan Dittberner
 */
public class SMIMEDecrypt {
    private final KeyStore keystore;

    /**
     * Create a new SMIMEDecrypt instance.
     * 
     * @param argKeystore
     *            keystore with private keys.
     */
    public SMIMEDecrypt(KeyStore argKeystore) {
        this.keystore = argKeystore;
    }

    /**
     * Decrypt an encrypted S/MIME message.
     * 
     * @param encrypted
     *            encrypted S/MIME message
     * @return decrypted MIME message
     * @throws Exception
     *             if an error occurs
     */
    public MimeMessage decryptMessage(MimeMessage encrypted)
            throws Exception {
        SMIMEEnveloped message = new SMIMEEnveloped(encrypted);

        RecipientInformationStore recinfos = message.getRecipientInfos();
        Enumeration<String> aliases = this.keystore.aliases();
        RecipientInformation recid = null;
        String alias = null;
        while ((recid == null) && aliases.hasMoreElements()) {
            alias = aliases.nextElement();
            if (this.keystore.isKeyEntry(alias)) {
                recid = recinfos.get(new JceKeyTransRecipientId(
                        (X509Certificate) this.keystore.getCertificate(alias)));
            }
        }
        if (recid == null) {
            throw new RuntimeException("No decryption key found");
        }

        JceKeyTransEnvelopedRecipient recipient = new JceKeyTransEnvelopedRecipient(
                (PrivateKey) this.keystore.getKey(alias, "changeit"
                        .toCharArray()));

        byte[] content = recid.getContent(recipient);

        MimeMessage decrypted = new MimeMessage(Session
                .getDefaultInstance(System.getProperties()),
                new ByteArrayInputStream(content));
        decrypted.saveChanges();
        return decrypted;
    }

}
