package info.dittberner.jcajceprovidertest.sectest;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.Provider;
import java.security.Security;

/**
 * Utility to list all available Security providers and their implemented algorithm names.
 *
 * @author Jan Dittberner &lt;<a href="mailto:jan@dittberner.info>jan@dittberner.info</a>&gt;
 */
public class ListAlgorithmNames {
    public static void main(String[] args) {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }

        for (Provider provider : Security.getProviders()) {
            System.out.println("Provider: " + provider.getName());
            for (Provider.Service service : provider.getServices()) {
                System.out.println("  Algorithm: " + service.getAlgorithm());
            }
        }

    }
}
