package info.dittberner.bcsmime_demo;

import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.cert.CertIOException;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.X509v3CertificateBuilder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.cert.CertificateException;
import java.util.Date;

/**
 * Created by jan on 12.10.14.
 */
public class KeyEntryData {
    private final X509CertificateHolder certificateHolder;
    KeyPair keyPair;

    public KeyEntryData(KeyPairGenerator kpg, String address) throws CertIOException, OperatorCreationException {
        this.keyPair = kpg.generateKeyPair();

        X500Name issuer = new X500Name(
                String.format("CN=Test Recipient,emailAddress=%s", address));
        //noinspection UnnecessaryLocalVariable
        X500Name subject = issuer;
        X509v3CertificateBuilder certificateBuilder = new JcaX509v3CertificateBuilder(
                issuer, BigInteger.valueOf(System.currentTimeMillis()),
                new Date(System.currentTimeMillis() - 50000), new Date(
                System.currentTimeMillis() + 50000), subject,
                keyPair.getPublic());
        certificateBuilder.addExtension(Extension.basicConstraints, true,
                new BasicConstraints(true));
        certificateBuilder.addExtension(Extension.keyUsage, true,
                new KeyUsage(KeyUsage.digitalSignature
                        | KeyUsage.keyEncipherment));
        certificateBuilder.addExtension(Extension.extendedKeyUsage, true,
                new ExtendedKeyUsage(KeyPurposeId.id_kp_emailProtection));
        certificateBuilder.addExtension(Extension.subjectAlternativeName,
                false, new GeneralNames(new GeneralName(
                        GeneralName.rfc822Name, address)));

        ContentSigner signer = new JcaContentSignerBuilder("SHA256WithRSA")
                .build(keyPair.getPrivate());
        this.certificateHolder = certificateBuilder.build(signer);
    }

    public java.security.cert.Certificate getCertificate() throws CertificateException, CertIOException, OperatorCreationException {
        return (new JcaX509CertificateConverter()).getCertificate(certificateHolder);
    }
}
