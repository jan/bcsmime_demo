/*
 * Copyright (c) 2011 Jan Dittberner
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */
package info.dittberner.bcsmime_demo;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.Security;
import java.security.cert.Certificate;
import java.util.logging.Logger;

import javax.mail.BodyPart;
import javax.mail.Message.RecipientType;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;

/**
 * Test Encryption and Decryption.
 *
 * @author Jan Dittberner &lt;<a href=
 *         "mailto:jan.dittberner@t-systems.com>jan.dittberner@t-systems.com</a>
 *         & g t ;
 */
public class EncryptDecryptTest {
    String[][] testEntries = new String[][]{
            new String[]{"test1", "testrecpt1@example.org", "Test Recipient 1"},
            new String[]{"test2", "testrecpt2@example.org", "Test Recipient 2"}
    };
    private KeyStore keystore;
    private Logger logger = Logger.getLogger(EncryptDecryptTest.class.getName());

    /**
     * {@inheritDoc}
     *
     * @see junit.framework.TestCase#setUp()
     */
    @Before
    public void setUp() throws Exception {
        if (Security.getProvider(BouncyCastleProvider.PROVIDER_NAME) == null) {
            Security.addProvider(new BouncyCastleProvider());
        }

        if (this.keystore == null) {
            this.keystore = KeyStore.getInstance("JKS");
            keystore.load(null, null);

            KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
            kpg.initialize(2048);

            for (String[] entry : testEntries) {
                KeyEntryData keyEntryData = new KeyEntryData(kpg, entry[1]);
                keystore.setKeyEntry(entry[0], keyEntryData.keyPair.getPrivate(), "changeit".toCharArray(), new Certificate[]{keyEntryData.getCertificate()});
            }

        }
    }

    private String messageAsString(MimeMessage message) throws IOException, MessagingException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        message.writeTo(byteArrayOutputStream);
        return byteArrayOutputStream.toString();
    }

    /**
     * Test of {@link SMIMEEncrypt} and {@link SMIMEDecrypt}.
     */
    @Test
    public void testEncryptDecryptMail() throws Exception {
        MimeMessage message = getNewMultipartMessage();
        assertNotNull(message);

        SMIMEEncrypt encrypt = new SMIMEEncrypt(keystore);
        MimeMessage encrypted = encrypt.encryptMessage(message);
        assertNotNull(encrypted);

        logger.info(messageAsString(encrypted));

        SMIMEDecrypt decrypt = new SMIMEDecrypt(keystore);
        MimeMessage decrypted = decrypt.decryptMessage(encrypted);
        assertNotNull(decrypted);

        logger.info(messageAsString(decrypted));
    }

    /**
     * Creates a new MimeMessage with one body part.
     *
     * @return MimeMessage instance
     * @throws MessagingException on error creating the message
     */
    private MimeMessage getNewMultipartMessage() throws MessagingException,
            IOException {
        Session mailsession = Session.getDefaultInstance(System.getProperties());
        MimeMessage message = new MimeMessage(mailsession);
        message.setFrom(new InternetAddress("testsender@example.org",
                "Test Sender"));
        for (String[] entry : testEntries) {
            message.addRecipient(RecipientType.TO, new InternetAddress(entry[1], entry[2]));
        }
        message.setSubject("Test subject");
        Multipart multipart = new MimeMultipart();
        BodyPart textPart = new MimeBodyPart();
        textPart.setText("Das ist ein Text");
        multipart.addBodyPart(textPart);
        message.setContent(multipart);
        return message;
    }
}